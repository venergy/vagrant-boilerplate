Vue.config.devtools = true;

var app = new Vue({
	el: '#app',
	data: {
		appName: 'vagrant boilerplate'
	},
	delimiters: ['${', '}'] //delimiters chaged to avoid conflict with twig
});