#! /bin/bash

#make sure this file is executible (chmod +x new-page.sh)

#get arguement as new page name
NAME=${1?Error: no filename given}

#get current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

#create new controller from template
if [ ! -f $DIR'/../page/controller/'$NAME'.php' ]; then
#copy template file
cp $DIR'/controller-template.php' $DIR'/../page/controller/'$NAME'.php'
#change page name text in template to new file name
sed -i '' "s/TEMPLATENAME/$NAME/" $DIR'/../page/controller/'$NAME'.php'
fi

#create new twig file from template
if [ ! -f $DIR'/../page/twig/'$NAME'.html.twig' ]; then
#copy template file
cp $DIR'/twig-template.html.twig' $DIR'/../page/twig/'$NAME'.html.twig'
fi