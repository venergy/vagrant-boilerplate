<?php

	//page specific variables
	$pageData = [
		'pageName' => 'TEMPLATENAME'
	];

	//init twig object render
	$twigRenderedContent = $twig->render($siteGlobals['page'].'.html.twig', [
		'global' => $siteGlobals,
		'page' => $pageData
	]);

	//load page specific css
	$pageCSS = [];

	//load page specific js
	$pageJS = [];