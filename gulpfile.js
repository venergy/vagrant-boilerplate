const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const uglyfly = require('gulp-uglyfly');
const browserSync = require('browser-sync').create();

function style(){
	return gulp.src('./css/*.css')
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(concat('global.min.css'))
	.pipe(gulp.dest('./dist'))
	.pipe(browserSync.stream());
}

function jscript(){
	return gulp.src(['./js/*.js'])
	.pipe(uglyfly())
	.pipe(concat('global.min.js'))
	.pipe(gulp.dest('./dist'))
	.pipe(browserSync.stream());
}

function watch(){
	browserSync.init({
			proxy: '192.168.56.10/'
			// server: {
			// 	baseDir: './'
			// }
	});
	gulp.watch('./css/*.css',style);
	gulp.watch('./js/*.js',jscript);
	gulp.watch([
		'./**/*.php',
		'./**/*.html.twig',
		// './css/lib/**/*.css',
		// './js/lib/**/*.js'
	]).on('change',browserSync.reload);
}

exports.style = style;
exports.jscript = jscript;
exports.watch = watch;

gulp.task('default',gulp.parallel(style,jscript,watch));