<?php
	$root = __DIR__;
	require_once "$root/init/init.php";
?>

<!doctype html>
<html lang="en">

    <head>

		<!-- meta -->
		<?= loadMeta(); ?>

		<!-- css -->
        <?= loadCSS(); ?>

	</head>

	<body>

		<!-- nav -->
		<?= loadNav(); ?>

		<!-- content -->
		<?= $twigRenderedContent; ?>

		<!-- js -->
		<?= loadJS(); ?>

	</body>
  
</html>