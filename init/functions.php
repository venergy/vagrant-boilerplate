<?php

	//format slug to name
	function formatSlugToName($pageName) {
		return ucwords(str_replace('-', ' ', $pageName));
	}

	//get first level path from URL
	function getInitialPath($path) {

		$path = trim($path,'/');

		if(str_contains($path,'/')) {
			$pos = stripos($path, '/');
			return substr($path,0,$pos);
		} else {
			return $path;
		}
	}

	//one way hash (sha256 returns 64 character hash)
	function hashString($input)
	{
		$salt = sha1(md5($input));
		return hash('sha256', $input.$salt);
	}

	function loadMeta() {
		global $root,$siteGlobals;
		require_once "$root/page/head.php";
	}

	function loadNav() {
		global $root,$siteGlobals, $siteNavigation;
		require_once "$root/page/navigation.php";
	}

	//load all css required
	function loadCSS() {
		global $root,$appVersion,$siteCSS,$pageCSS;

		//add page specific css if exists (declared in page controller)
		if(isset($pageCSS) && !empty($pageCSS)) {
			for($i=0;$i<count($pageCSS);$i++) {
				array_push($siteCSS, $pageCSS[$i]);
			}
		}

		$css = "";

		//create css tags
		if(isset($siteCSS) && !empty($siteCSS)) {
			for($i=0;$i<count($siteCSS);$i++) {
				$css .= '<link ';
				foreach($siteCSS[$i] AS $key => $value) {
					$css .= $key.'="'.$value.'" ';
				}
				$css .= 'rel="stylesheet" >';
			}
		}

		return $css;
	}

	//load all javascript required
	function loadJS() {
		global $siteGlobals,$root,$appVersion,$siteJS,$pageJS;

		//add page specific js  to the end of siteJS if exists (declared in page controller)
		if(isset($pageJS) && !empty($pageJS)) {
			for($i=0;$i<count($pageJS);$i++) {
				array_push($siteJS, $pageJS[$i]);
			}
		}

		$js = "";

		//load site specific js (declared in config)
		if(isset($siteJS) && !empty($siteJS)) {
			for($i=0;$i<count($siteJS);$i++) {
				$js .= '<script ';
				foreach($siteJS[$i] AS $key => $value) {
					$js .= $key.'="'.$value.'" ';
				}
				$js .= '></script>';
			}
		}

		return $js;
	}

	//send email via PHP Mailer
	function sendMailer(Array $emailData) {

		global $mailer;

		try
		{
			//options
			$mailer->isHTML(true); //set email format to HTML

			//recipients
			$mailer->setFrom($emailData['mailFromEmail'],$emailData['mailFromName']);
			$mailer->addAddress($emailData['mailToEmail']); // Name is optional
			$mailer->addReplyTo($emailData['mailReplyEmail'], $emailData['mailReplyName']);

			//content
			$mailer->Subject = $emailData['mailSubject'];
			$mailer->Body = $emailData['mailBody']; //html
			$mailer->AltBody = $emailData['mailBodyAlt']; //text
			
			if(isset($emailData['mailToCC'])) {
				$mailer->addBCC($emailData['mailToCC']);
			}

			//send
			if($emailData['mailSend']) {
				$mailer->send();
			}
			
			return true;

		} catch (Exception $e) {
			return $mailer->ErrorInfo;
		}
	}
