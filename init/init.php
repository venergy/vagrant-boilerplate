<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	//load global config and shared functions
	require_once "$root/init/config.php";
	require_once "$root/init/functions.php";
	//load all composer modules
	require_once "$root/vendor/autoload.php";

	//site global variables
	$siteGlobals['siteURL'] = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
	$siteGlobals['currentPageURL'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

	//add path variables to global variables array
	if (isset($_GET['path'])) {
		$path = $_GET['path'];
		$path = rtrim($path, '/');
		$path = filter_var($path, FILTER_SANITIZE_URL);
		$pathSections = explode('/', $path);
		for($i=0;$i<count($pathSections);$i++) {
			$siteGlobalsVarValue = $i + 1;
			$siteGlobals['value'.$siteGlobalsVarValue] = $pathSections[$i];
		}
	} else {
		$siteGlobals['value1'] = 'home';
	}

	//declare requested page
	$siteGlobals['page'] = $siteGlobals['value1'];

	//declare environment details
	$siteGlobals['serverVersion'] = shell_exec('apache2 -v 2>&1');
	$siteGlobals['phpVersion'] = phpversion();
	$siteGlobals['mysqlVersion'] = shell_exec('mysql --version');

	//instantiate .env
	$dotenv = Dotenv\Dotenv::createImmutable($root);
	$dotenv->load();

	//load phpmailer
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\PHPMailer;
	require "$root/vendor/phpmailer/phpmailer/src/Exception.php";
	require "$root/vendor/phpmailer/phpmailer/src/PHPMailer.php";
	require "$root/vendor/phpmailer/phpmailer/src/SMTP.php";

	$mailer = new PHPMailer(true);

	//instantiate pdo
	require_once "$root/lib/pdo.php";

	//instantiate twig
	$loader = new \Twig\Loader\FilesystemLoader('page/twig');
	$twig = new \Twig\Environment($loader, [
		'cache' => false,
	]);

	//check if requested page controller exists else 404
	if(!file_exists("$root/page/controller/".$siteGlobals['page'].".php")) {
		$siteGlobals['page'] = '404';
	}

	//load page controller
	require_once "$root/page/controller/".$siteGlobals['page'].".php";
	