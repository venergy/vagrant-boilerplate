<?php

	$appVersion = "1.1.1";

	//site globals declaration
	$siteGlobals = [
		'siteName' => "Vagrant Boilerplate",
		'siteShortDescription' => "All in one boilerplate LAMP Vagrant stack.",
		'siteLongDescription' => "This is an all in one boilerplate for a LAMP Vagrant stack."
	];

	//navigation and routes
	$siteNavigation = [
		"Home" => ['path' => "/home/is/where/the/heart/is"],
		"Database" => ['path' => "/database/pdo/connected"],
		"Vue" => ['path' => "/vue/loaded/this/page/only"],
		"404" => ['path' => "/404/beats/per/minute"],
		"Dropdown" => ['submenu' => [
			"Jose" => ['path' => "/jose"],
			"Hose B" => ['path' => "/hose-b"],
		]]
	];

	//site CSS
	$siteCSS = [
		[
			'href' => "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css",
			'integrity' => "sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1",
			'crossorigin' => "anonymous"
		],
		[
			'href' => "/dist/global.min.css?v=".$appVersion
		]
	];

	//site JS
	$siteJS = [
		[
			'src' => "https://code.jquery.com/jquery-3.4.1.min.js",
			'integrity' => "sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=",
			'crossorigin' => "anonymous"
		],
		[
			'src' => "https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js",
			'integrity' => "sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU",
			'crossorigin' => "anonymous"
		],
		[
			'src' => "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js",
			'integrity' => "sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj",
			'crossorigin' => "anonymous"
		],
		[
			'src' => "/dist/global.min.js?v=".$appVersion,
		]
	];