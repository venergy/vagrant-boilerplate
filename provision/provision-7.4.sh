#!/usr/bin/env bash

echo -e "\n============================================\n"
echo -e "Provisioning. Ubuntu LAMP w/ PHP 7.4\n"
echo -e "============================================\n"

echo -e "\n --> Adding repositories.\n\n"
sudo apt-get --assume-yes install software-properties-common python-software-properties
sudo add-apt-repository -y ppa:ondrej/php

echo -e "\n --> Updating repositories.\n\n"
sudo apt-get update

echo -e "\n --> Installing apache2.\n\n"
sudo apt-get --assume-yes install apache2

# MySql Variables
DBHOST=localhost
DBNAME=vagrant
DBUSER=root
DBPASSWD=root

echo -e "\n --> Installing mysql-server.\n\n"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
sudo apt-get -y install mysql-server

echo -e "\n --> Installing database.\n\n"
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DBNAME"
mysql -uroot -p$DBPASSWD -e "grant all privileges on $DBNAME.* to '$DBUSER'@'$DBHOST' identified by '$DBPASSWD'"

echo -e "\n --> Importing database.\n\n"
sudo cp /var/www/html/docs/vagrant.sql vagrant.sql
mysql -uroot -p$DBPASSWD $DBNAME < vagrant.sql

echo -e "\n --> Installing php.\n\n"
sudo apt-get --assume-yes install php7.4 php7.4-cli php7.4-common
sudo apt-get --assume-yes install php7.4-curl php7.4-gd php7.4-json php7.4-mbstring php7.4-intl php7.4-mysql php7.4-xml php7.4-zip
sudo apt-get --assume-yes install libapache2-mod-php

echo -e "\n --> Starting apache.\n\n"
sudo /etc/init.d/apache2 start

echo -e "\n --> Enable mod rewrite.\n\n"
cd /etc/apache2
sudo a2enmod rewrite
sudo cp /var/www/html/provision/apache2.conf /etc/apache2/apache2.conf
sudo service apache2 restart

echo -e "\n --> Enable xdebug.\n\n"
sudo apt-get --assume-yes install php7.4-xdebug
sudo cp /var/www/html/provision/php7.4.ini /etc/php/7.4/apache2/php.ini
sudo service apache2 restart

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

echo -e "\n --> Provisioning complete."
echo -e "\n --> Site URL:"
echo -e "\n --> ${RED}http://192.168.56.10${GREEN}"

echo -e "\n --> MySQL DB:"
echo -e "\n --> login:root psw:root"
echo -e "\n --> Check: sudo nano ~/.ssh/known_hosts for existing ssh entry to 127.0.0.1\n\n"