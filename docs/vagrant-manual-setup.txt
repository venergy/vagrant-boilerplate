FOLLOW: URL 'Setting Up a LAMP....' https://dev.to/boringbdon/setting-up-a-lamp-server-on-a-vagrant-machine-ppi

VAGRANT

	cd ~/Desktop/our_vagrant_machine

	vagrant init ubuntu/xenial64

	vagrant up

	vagrant ssh
		sudo apt-get install apache2
		sudo apt-get install mysql-server # This will ask for a root password, I leave it blank. (MV: set it to root)
		sudo apt-get install php
		sudo apt-get install php7.0-mysql

UNCOMMENT the following lines in your Vagrantfile
	config.vm.network "forwarded_port", guest: 80, host: 8080
	config.vm.network "private_network", ip: "192.168.33.10"

	vagrant reload

	vagrant ssh

	sudo /etc/init.d/apache2 start

UNCOMMENT the following line in your Vagrantfile
	config.vm.synced_folder "../data", "/vagrant_data"
	change "../data" to our site folder and "/vagrant_data" to the path where our files will live within the Vagrant Machine.
	Change to:
	config.vm.synced_folder "./", "/var/www/html/"

	vagrant reload

MYSQL

	vagrant ssh

	sudo mysql or (mysql -u root -p) psw:root
		CREATE USER 'ubuntu';
		GRANT ALL PRIVILEGES ON *.* TO ubuntu;
		exit;

SEQUEL PRO

	vagrant ssh-config

	Open Sequel Pro, navigate to the SSH.
	Name enter whatever you want
	MySQL Host enter 127.0.0.1
	username enter ubuntu
	password blank (MV: root)
	database blank (MV: root)
	port blank

	SSH Host enter 127.0.0.1
	SSH User enter vagrant
	SSH Key click the little key icon in the field, navigate to our_vagrant_machine/.vagrant/machines/default/virtualbox/ and select private_key.
	SSH Port enter 2222

	and now press Connect and we should now be logged into our database.

	If Sequel pro fails to connect with a ssh error you might need to go to:
	sudo nano ~/.ssh/known_hosts
	and delete the record for 127.0.0.1

VIRTUAL HOSTS

	Add hosts record (/etc/hosts) 192.168.33.10 vagrant.test

	vagrant ssh
		cd /etc/apache2/sites-available/
		sudo nano vagrant.test

			add the following text and save:
			<VirtualHost *:80>
			ServerAdmin webmaster@localhost
			ServerName vagrant.test
			DocumentRoot /var/www/html/
			</VirtualHost>

		sudo a2ensite hello.world.conf
		sudo service apache2 reload

ENABLE MOD REWRITE (.htaccess)

	Vagrant ssh
		cd /etc/apache2
		sudo a2enmod rewrite
		sudo service apache2 restart

	sudo nano apache2.conf

		Change:
		<Directory /var/www/>
			Options Indexes FollowSymLinks
			AllowOverride None
			Require all granted
		</Directory>

		To:
		<Directory /var/www/>
			Options Indexes FollowSymLinks MultiViews
			AllowOverride All
			Require all granted
		</Directory>

		Write Out (control O)
		Exit (control X)

	sudo service apache2 restart

ENABLE X-DEBUG MODE

	sudo apt-get install php-xdebug

	/etc/php/7.0/apache2/php.ini

		Add to the bottom of the php.ini:

		zend_extension = /usr/lib/php/20151012/xdebug.so

		[xdebug]
		xdebug.remote_enable = true
		xdebug.remote_autostart = true
		xdebug.remote_host = 10.0.2.2
		xdebug.remote_port = 9000
		xdebug.remote_log = /var/log/xdebug.log
		xdebug.max_nesting_level = 1000

	sudo service apache2 restart

	php -v (to confirm)

ENABLE DEBUG VIA VS CODE

	launch.json

		{
			// Use IntelliSense to learn about possible attributes.
			// Hover to view descriptions of existing attributes.
			// For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
			"version": "0.2.0",
			"configurations": [
				{
					"type": "php",
					"request": "launch",
					"name": "Listen for XDebug Vagrant",
					"pathMappings": {
					"/var/www/html": "${workspaceRoot}",
					},
					"port": 9000,
					"log": false,
				}, 
			]
		}


PROVISIONING - TODO: https://gist.github.com/ronanmccoy/b20b6301848783066a6b23e95def5999