<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- favicon -->
<link rel="shortcut icon" type="image/x-icon" href="/img/favicon.png">

 <!-- canonical -->
 <link rel="canonical" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" />

 <!-- page title -->
<?php $pageTitle = (isset($siteGlobals['page']) && $siteGlobals['page'] != '') ? $siteGlobals['siteName'].' | '.formatSlugToName($siteGlobals['page']) : $siteGlobals['siteName']; ?>

<title><?= $pageTitle; ?></title>

<!--facebook meta og-->
<meta property="og:title" content="<?= $siteGlobals['siteShortDescription']; ?>"/>
<meta property="og:url" content="<?= $siteGlobals['siteURL']; ?>"/>
<meta property="og:image" content="<?= $siteGlobals['siteURL']; ?>img/fb_image.jpg"/>
<meta property="og:site_name" content="<?= $siteGlobals['siteName']; ?>"/>
<meta property="og:description" content="<?= $siteGlobals['siteLongDescription']; ?>"/>

<!-- twitter -->
<meta property="twitter:url" content="<?= $siteGlobals['currentPageURL']; ?>">
<meta property="twitter:title" content="<?= $pageTitle; ?>">
<meta property="twitter:description" content="<?= $siteGlobals['siteLongDescription']; ?>">