<?php

	//page specific variables
	$pageData = [
		'pageName' => 'Index'
	];

	//init twig object render
	$twigRenderedContent = $twig->render($siteGlobals['page'].'.html.twig', [
		'global' => $siteGlobals,
		'page' => $pageData
	]);

	//load page specific css
	$pageCSS = [
		[
			'href' => "/css/lib/vendor.css?v=".$appVersion,
		]
	];

	//load page specific js
	$pageJS = [
		[
			'src' => "/js/lib/vendor.js?v=".$appVersion,
		]
	];

	//PHP mailer eg.
	sendMailer([
		'mailFromEmail' => $_ENV['SEND_FROM_ADDRESS'],
		'mailFromName' => $_ENV['SEND_FROM_NAME'],
		'mailToEmail' => "you@email.com",
		'mailReplyEmail' => $_ENV['SEND_FROM_ADDRESS'], //same as mailFromEmail
		'mailReplyName' => $_ENV['SEND_FROM_NAME'], //same as mailFromName
		'mailSubject' => "This is Vagrant Boilerplate",
		'mailBody' => "<b>Hello World!</b>",
		'mailBodyAlt' => "Hello World!",
		'mailSend' => false
	]);
