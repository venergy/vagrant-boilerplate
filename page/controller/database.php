<?php

	//db: retrieve all from vagrant_test table
	$query = "SELECT * FROM vagrant_test";
    $prepared = $pdo->prepare($query);
    $prepared->execute([]);
    $result = $prepared->fetchAll();

	//page specific variables
	$pageData = [
		'pageName' => 'Database',
		'result' => $result
	];

	//init twig object render
	$twigRenderedContent = $twig->render($siteGlobals['page'].'.html.twig', [
		'global' => $siteGlobals,
		'page' => $pageData
	]);
