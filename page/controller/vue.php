<?php

	//page specific variables
	$pageData = [
		'pageName' => 'Vue'
	];

	//init twig object render
	$twigRenderedContent = $twig->render($siteGlobals['page'].'.html.twig', [
		'global' => $siteGlobals,
		'page' => $pageData
	]);

	//load page specific js
	$pageJS = [
		[
			//vue js
			'src' => "https://cdn.jsdelivr.net/npm/vue@2.6.12",
		],
		[
			'src' => "/js/page/vue.js"
		]
	];