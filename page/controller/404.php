<?php

	//page specific variables
	$pageData = [
		'pageName' => 'Page not found'
	];

	//init twig object render
	$twigRenderedContent = $twig->render($siteGlobals['page'].'.html.twig', [
		'global' => $siteGlobals,
		'page' => $pageData
	]);