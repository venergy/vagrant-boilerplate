<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="/home"><?= $siteGlobals['siteName']; ?></a>

    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">

	  	<?php
			foreach($siteNavigation AS $key => $value) {

				$isSubmenu = (isset($value['submenu']) && count($value['submenu']) > 0) ? true : false;

				if($isSubmenu) {

					echo '<li class="nav-item dropdown">';
					echo '<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Dropdown</a>';
					echo '<ul class="dropdown-menu" aria-labelledby="navbarDropdown">';

					foreach($value['submenu'] AS $subKey => $subValue) {
						$subIsActive = (isset($subValue['path']) && strtolower($siteGlobals['page']) == getInitialPath($subValue['path'])) ? 'active' : '';
						$subLabel = $subKey;
						$subPath = (isset($subValue['path'])) ? $subValue['path'] : '#';

						echo '<li><a class="dropdown-item '.$subIsActive.'" href="'.$subPath.'">'.$subLabel.'</a></li>';
					}
					
					echo '</ul>';
					echo '</li>';

				} else {
					
					$isActive = (isset($value['path']) && strtolower($siteGlobals['page']) == getInitialPath($value['path'])) ? 'active' : '';
					$label = $key;
					$path = (isset($value['path'])) ? $value['path'] : '#';

					echo '<li class="nav-item">';
					echo '<a class="nav-link '.$isActive.'" href="'.$path.'">'.$label.'</a>';
					echo '</li>';
				}
				
			}
		?>

      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" id="site-search-field">
        <button class="btn btn-outline-secondary" id="site-search-button">Search</button>
      </form>
    </div>
  </div>

</nav>