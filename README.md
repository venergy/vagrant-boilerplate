# Vagrant Boilerplate

Vagrant ubuntu/bionic64 LAMP box & custom MVC/twig framework. 

Includes:

* Apache
* MySql
* PHP 7.4
	* Mod-Rewrite
	* xDebug
	* PHPMailer
	* Twig
	* PhpDotEnv
	* PDO
* NPM
	* Gulp
	* Clean-CSS
	* Concat
	* Unlyfly
	* Browser Sync
* jQuery
* Bootstrap
* Vue

**Install:**

Install Virtual Box
https://www.virtualbox.org/

Install Vagrant
https://www.vagrantup.com/

Install NodeJS
https://nodejs.org/en/

Install Composer
https://getcomposer.org/

**In The Terminal:**

	composer install
	if composer complains about a lower version of php use: (composer install --ignore-platform-reqs)

	npm install

	vagrant up

If vagrant throws an error (error: Failed to create the host-only adapter) then go to System Preferences > Security & Privacy Then hit the "Allow" button to let Oracle (VirtualBox) load.

**Database Access:**

	host: 127.0.0.1
	username: root
	password: root
	port blank

	SSH host; 127.0.0.1
	SSH username: vagrant
	SSH Key: vagrant-boilerplate/.vagrant/machines/default/virtualbox/private_key
	SSH Port enter 2222

If DB client fails to connect with an ssh error you might need to:
sudo nano ~/.ssh/known_hosts
and delete the record for 127.0.0.1

**In The Terminal:**

	gulp

**Enable Debug Via VS Code**

launch.json

	{
		// Use IntelliSense to learn about possible attributes.
		// Hover to view descriptions of existing attributes.
		// For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
		"version": "0.2.0",
		"configurations": [
			{
				"type": "php",
				"request": "launch",
				"name": "Listen for XDebug Vagrant",
				"pathMappings": {
				"/var/www/html": "${workspaceRoot}",
				},
				"port": 9003,
				"log": false,
			}, 
		]
	}
